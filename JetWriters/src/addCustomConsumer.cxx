#include "JetWriters/addCustomConsumer.h"
#include "FlavorTagDiscriminants/customGetter.h"

#include "xAODTracking/TrackParticle.h"
#include "xAODCaloEvent/CaloCluster.h"

namespace {

  using namespace jetwriters::detail;

  // wrapper class to convert the things we have in
  // FlavorTagDiscriminants into something that HDF5Utils can read.
  template <typename T>
  class CustomSeqWrapper
  {
    using SeqGetter = decltype(
      FlavorTagDiscriminants::customSequenceGetterWithDeps(
        std::declval<std::string>(),
        std::declval<std::string>()
        )
      );
  public:
    CustomSeqWrapper(SeqGetter g): m_getter(g.first) {
    }
    T operator()(const JetPair<xAOD::TrackParticle>& t) {
      auto output_vec = m_getter(*t.second, {t.first});
      return output_vec.at(0);
    }
  private:
    SeqGetter::first_type m_getter;
  };

  const std::string g_ip_prefix = "ip_prefix";

}

namespace jetwriters::detail {

  template <>
  JetGetter<xAOD::TrackParticle, float> getFloat<xAOD::TrackParticle>(
    const std::string& name,
    EdmNameMap& edmName)
  {

    std::string prefix = edmName(g_ip_prefix);
    if (prefix == g_ip_prefix) {
      throw std::runtime_error(
        "must specify 'ip_prefix' to use '" + name + "'");
    }

    if (name == "z0RelativeToBeamspotUncertainty") {
      return [](const JetPair<xAOD::TrackParticle>& t) -> float {
        return std::sqrt(
          t.first->definingParametersCovMatrixDiagVec().at(1));
      };
    } else if (name == "d0RelativeToBeamspot"){
      return [](const JetPair<xAOD::TrackParticle>& t) -> float {
        return t.first->d0();
      };
    } else if (name == "d0RelativeToBeamspotSignificance"){
      return [](const JetPair<xAOD::TrackParticle>& t) -> float {
        return t.first->d0() / std::sqrt(
          t.first->definingParametersCovMatrixDiagVec().at(0));
      };
    } else {
      // most of the custom getters can be grabbed from Athena
      auto getter = FlavorTagDiscriminants::customSequenceGetterWithDeps(
        name, prefix);
      // note that within FlavorTagDiscriminants all the floats are
      // double we're truncating here because that precision is
      // probably not needed
      CustomSeqWrapper<float> wrapped(getter);
      return wrapped;
    }
  }

  template <>
  JetGetter<xAOD::CaloCluster, float> getFloat<xAOD::CaloCluster>(
    const std::string& name,
    EdmNameMap& edmName)
  {

    // doesn't compile withouth using edmName somehow
    std::string prefix = edmName(g_ip_prefix);

    if (name == "etaSize") {
      return [](const JetPair<xAOD::CaloCluster>& t) -> float {
        return t.first->getClusterEtaSize();
      };
    } else if (name == "phiSize"){
      return [](const JetPair<xAOD::CaloCluster>& t) -> float {
        return t.first->getClusterPhiSize();
      };
    } else if (name == "ncells"){
      return [](const JetPair<xAOD::CaloCluster>& t) -> float {
        return t.first->numberCells();
      };
    } else if (name == "FRAC_HAD"){
      return [](const JetPair<xAOD::CaloCluster>& t) -> float {
        double value;
        t.first->retrieveMoment(xAOD::CaloCluster_v1::MomentType::ENG_CALIB_FRAC_HAD, value);
        return (float) value;
      };
    } else {
      return std::nullopt;;
    }
  }

  EdmNameMap::EdmNameMap(const std::map<std::string, std::string>& in):
    m_remap(in)
  {
  }
  std::string EdmNameMap::operator()(const std::string& in)
  {
    if (m_remap.count(in)) {
      m_used.insert(in);
      return m_remap.at(in);
    }
    return in;
  }
  // checks to ensure that all varialbe name replacements are used
  void EdmNameMap::checkRemaining()
  {
    auto edm_name = m_remap;
    for (const auto& k: m_used) edm_name.erase(k);
    // ip prefix is kind of a special case: it might be a required
    // parameter even if it's not used to make a variable
    edm_name.erase(g_ip_prefix);
    if (!edm_name.empty()) {
      std::string error = "Found unused name substitutions: ";
      std::string unused;
      for (auto& name_pair: edm_name) {
        if (!unused.empty()) unused.append(", ");
        unused.append(
          "'" + name_pair.first + "'->'" + name_pair.second + "'");
      }
      throw std::runtime_error(error + unused);
    }
  }


}
