#!/usr/bin/env python

"""
Dump large-R jets with VR subjet scores

"""

import sys
from FTagDumper import dumper

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory


def orCfg(name, jets, tracks='GhostTrack'):
    ca = ComponentAccumulator()
    ca.addEventAlgo(
        CompFactory.TrackFlowOverlapRemovalAlg(
            name=name,
            Tracks=f'{jets}.{tracks}',
            Constituents=f'{jets}.constituentLinks',
            OutTracks=f'{jets}.nonConstituentGhostTracks'
        )
    )
    return ca

def run():

    args = dumper.base_parser(__doc__).parse_args()

    from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
    dumper.update_cfgFlags(cfgFlags, args)
    cfgFlags.lock()

    ca = dumper.getMainConfig(cfgFlags, args)
    ca.merge(orCfg('nonConstituentAdder', 'AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets', 'GhostTrack'))

    sub = ComponentAccumulator()

    BTaggingCollection = 'BTagging_AntiKtVR30Rmax4Rmin02Track'
    TrackCollection = 'AntiKtVR30Rmax4Rmin02PV0TrackJets'

    tools_info = [
        (
            CompFactory.FlavorTagDiscriminants.GNNTool,
            "GN2v00",
            "dev/BTagging/20230307/gn2v00/antiktvr30rmax4rmin02track/network.onnx",
            {},
        ),
        (
            CompFactory.FlavorTagDiscriminants.DL2Tool,
            "dipsLoose",
            "dev/BTagging/20230208/dipsLoose/antiktvr30rmax4rmin02track/network.json",
            {},
        ),
        (
            CompFactory.FlavorTagDiscriminants.DL2Tool,
            "DL1dv01_VR",
            "dev/BTagging/20230307/DL1dv01/antiktvr30rmax4rmin02track/network.json",
            {
                f"DL1dv01_{x}": f"DL1dv01_VR_{x}"
                for x in ["pb", "pc", "pu", "isDefaults"]
            },
        ),
    ]

    for tool_constructor, tool_name, nn_file, variable_remapping in tools_info:
        tool = tool_constructor(nnFile=nn_file, variableRemapping=variable_remapping)
        decor_alg = CompFactory.FlavorTagDiscriminants.BTagDecoratorAlg(
            name=tool_name,
            container=BTaggingCollection,
            constituentContainer=TrackCollection,
            decorator=tool,
        )
        sub.addEventAlgo(decor_alg)

    ca.merge(sub)
    ca.merge(dumper.getDumperConfig(args))
    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
