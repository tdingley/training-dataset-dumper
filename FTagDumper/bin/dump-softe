#!/usr/bin/env python

"""

A good example of scheduling decoration algorithms alongside the dumper.

This script is similar to dump-single-btag, but with the 
following changes:

    - JetDumperElectronAlg adds 19 soft electron variables 
      to the b-tagging object
    - SoftElectronDecoratorAlg decorates reconstructed electrons 
      with additional info
    - SoftElectronDecoratorAlg decorates reconstructed electrons 
      with truth info

"""

import sys
from FTagDumper import dumper

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory


def run():

    args = dumper.base_parser(__doc__).parse_args()

    from AthenaConfiguration.AllConfigFlags import ConfigFlags as cfgFlags
    dumper.update_cfgFlags(cfgFlags, args)
    cfgFlags.lock()

    ca = dumper.getMainConfig(cfgFlags, args)

    # add some detailed truth info not in the physval sample
    sub = ComponentAccumulator()
    trackTruthOriginTool = CompFactory.InDet.InDetTrackTruthOriginTool(isFullPileUpTruth=False)
    sub.addEventAlgo(CompFactory.FlavorTagDiscriminants.TruthParticleDecoratorAlg(
        'TruthParticleDecoratorAlg', trackTruthOriginTool=trackTruthOriginTool,
    ))
    sub.addEventAlgo(CompFactory.FlavorTagDiscriminants.TrackTruthDecoratorAlg(
        'TrackTruthDecoratorAlg', trackTruthOriginTool=trackTruthOriginTool,
    ))
    ca.merge(sub)

    # add some soft electron stuff
    sub = ComponentAccumulator()
    sub.addEventAlgo(CompFactory.FlavorTagDiscriminants.SoftElectronDecoratorAlg('SoftElectronDecoratorAlg'))
    sub.addEventAlgo(CompFactory.FlavorTagDiscriminants.SoftElectronTruthDecoratorAlg('SoftElectronTruthDecoratorAlg'))
    ca.merge(sub)

    ca.merge(dumper.getDumperConfig(args))
    return ca.run()


if __name__ == '__main__':
    code = run()
    sys.exit(0 if code.isSuccess() else 1)
