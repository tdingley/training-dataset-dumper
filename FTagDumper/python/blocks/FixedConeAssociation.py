from dataclasses import dataclass

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
from ParticleJetTools.JetParticleAssociationAlgConfig import JetParticleFixedConeAssociationAlgCfg

from .BaseBlock import BaseBlock

@dataclass
class FixedConeAssociation(BaseBlock):
    """
    Run a DR=0.5 fixed cone track association.
    The fixed cone association is used for the training of the nntc model.

    To run the nntc, you can use the following configuration: EMPFlow_fixedcone.json.
    The size of the cone is parametrized by the fixedConeRadius parameter.
    """

    def to_ca(self):    
        ca = ComponentAccumulator()
        an = 'FixedCone_Tracks'
        temp_jets='AntiKt4EMPFlowJets'
        fs_tracks='InDetTrackParticles'
        bc='BTagging_AntiKt4EMPFlow'
        trackOnJetDecorator = f'{temp_jets}.{an}ForBTagging'

        ca.merge(JetParticleFixedConeAssociationAlgCfg(
            self.cfgFlags,
            fixedConeRadius=0.5,
            JetCollection=temp_jets,
            InputParticleCollection=fs_tracks,
            OutputParticleDecoration=trackOnJetDecorator.split('.')[-1]
        ))
        Copier = CompFactory.FlavorTagDiscriminants.BTagTrackLinkCopyAlg
        copier = Copier(
            'TrackCopier',
            jetTracks=trackOnJetDecorator,
            btagTracks=f'{bc}.{an}',
            jetLinkName=f'{bc}.jetLink'
        )
        ca.addEventAlgo(copier)
        return ca
