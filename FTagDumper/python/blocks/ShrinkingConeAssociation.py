from dataclasses import dataclass

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from ParticleJetTools.JetParticleAssociationAlgConfig import JetParticleAssociationAlgCfg

from .BaseBlock import BaseBlock

@dataclass
class ShrinkingConeAssociation(BaseBlock):
    """
    Associate tracks to jets using the shrinking cone algorithm.

    More info: https://ftag.docs.cern.ch/algorithms/taggers/inputs/
    """
    jet_collection: str = None
    track_collection: str = "InDetTrackParticles"
    link_name: str = "DeltaRTracks"

    def __post_init__(self):
        if self.jet_collection is None:
            self.jet_collection = self.dumper_config["jet_collection"]

    def to_ca(self):
        ca = ComponentAccumulator()
        ca.merge(JetParticleAssociationAlgCfg(
            self.cfgFlags,
            JetCollection=self.jet_collection,
            InputParticleCollection=self.track_collection,
            OutputParticleDecoration=self.link_name,
        ))

        return ca
