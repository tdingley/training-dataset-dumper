from dataclasses import dataclass

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from .BaseBlock import BaseBlock

@dataclass
class JetMatcher(BaseBlock):
    '''Matches jets from a single source collection to jets in a target collection. For more
    details, see
    -   [JetMatcherAlg.cxx](https://gitlab.cern.ch/atlas-flavor-tagging-tools/training-dataset-dumper/-/blob/main/FTagDumper/src/JetMatcherAlg.cxx)
    
    Parameters
    ----------
    source_jets : str
        The name of the source jet collection to match.
    source_name : str, optional
        The name of a singular jet in the source collection. Used for generating the variables
        'deltaRTo{source_name}' and 'deltaPtTo{source_name}'. If not provided, it will be
        the same as the source collection.
    target_collection : str, optional
        The name of the target jet collection to match to. If not provided, it will be taken 
        from the dumper config.
    floats_to_copy : list[str], optional
        List of float variables to copy from the source jets to the target jets
    ints_to_copy : list[str], optional
        List of int variables to copy from the source jets to the target jets
    pt_priority_with_delta_r : float, optional
        The priority of the pt variable when matching jets based on deltaR.
        Disabled by default.
    '''
    

    source_jets: str
    source_name: str = None
    target_collection: str = None
    floats_to_copy: list[str] = None
    ints_to_copy: list[str] = None
    pt_priority_with_delta_r: float = -1
    source_minimum_pt: float = 0.0

    def __post_init__(self):

        if self.source_name is None:
            self.source_name = self.source_jets
        if self.target_collection is None:
            self.target_collection = self.dumper_config["jet_collection"]
        if self.floats_to_copy is None:
            self.floats_to_copy = []
        if self.ints_to_copy is None:
            self.ints_to_copy = []
        if not (self.floats_to_copy or self.ints_to_copy):
            raise ValueError('You must provide at least one float or int to copy')
        

    def to_ca(self):
        ca = ComponentAccumulator()
        dr_str = f'deltaRTo{self.source_name}'
        dpt_str = f'deltaPtTo{self.source_name}'
        ca.addEventAlgo(
            CompFactory.JetMatcherAlg(
                f'{self.source_name}To{self.target_collection}CopyAlg',
                targetJet=self.target_collection,
                sourceJets=[self.source_jets],
                floatsToCopy={f:f for f in self.floats_to_copy},
                intsToCopy={i:i for i in self.ints_to_copy},
                dR=dr_str,
                dPt=dpt_str,
                ptPriorityWithDeltaR=self.pt_priority_with_delta_r,
                sourceMinimumPt=self.source_minimum_pt
            )
        )
        
        return ca