from dataclasses import dataclass

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from .BaseBlock import BaseBlock

@dataclass
class BTagJetLinker(BaseBlock):
    """Matches a jet collection to a b-tagging collection.

    See:
        - [BTagToJetLinkerAlg.h](https://gitlab.cern.ch/npond/athena/-/blob/master/PhysicsAnalysis/JetTagging/FlavorTagDiscriminants/FlavorTagDiscriminants/BTagToJetLinkerAlg.h)

    Parameters
    ----------
    jet_collection : str
        The name of the jet collection to match. If not provided, it will be taken from the dumper config.
    old_link : str
        The name of the old link to match. If not provided, it will be set to `{jet_collection}Jets.btaggingLink`.
    new_link : str
        The name of the new link to match. If not provided, it will be set to `BTagging_{jet_collection}.jetLink`.
    """

    jet_collection: str = None
    old_link: str = None
    new_link: str = None

    def __post_init__(self):
        if self.jet_collection is None:
            self.jet_collection = self.dumper_config["jet_collection"]
            self.jet_collection = self.jet_collection.replace('Jets','')
        if self.old_link is None:
            self.old_link = f'{self.jet_collection}Jets.btaggingLink'
        if self.new_link is None:
            self.new_link = f'BTagging_{self.jet_collection}.jetLink'
        

    def to_ca(self):
        ca = ComponentAccumulator()
        ca.addEventAlgo(
            CompFactory.BTagToJetLinkerAlg(
                f'jetToBTagFor{self.jet_collection}',
                newLink=self.new_link,
                oldLink=self.old_link
            )
        )
        
        return ca