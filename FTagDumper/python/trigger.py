from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator

# needed for trigger decision tool
from AthenaServices.MetaDataSvcConfig import MetaDataSvcCfg

# tools!
from TrigDecisionTool.TrigDecisionToolConfig import TrigDecisionToolCfg
from ParticleJetTools.JetParticleAssociationAlgConfig import (
    JetParticleAssociationAlgCfg)
from ParticleJetTools.ParticleJetToolsConfig import (
    getJetDeltaRFlavorLabelTool)

##############################################################################
######################## general configuration ###############################
##############################################################################

def getJobConfig(combined_cfg):
    # Split dupmer and trig job steering options
    if "dumper" in combined_cfg:
        dumper_cfg = combined_cfg.pop("dumper")
    else:
        raise("ERROR: Trying to load trigger configuration without the \"dumper\" group!")

    # Define available trig job options with default values
    trig_cfg = {}

    for k in ['mc21','mc24','data','offline_jets','do_track_matching','emtopo_jets','additional_chains']:
        if k not in combined_cfg.keys():
            raise ValueError(k,'has not been configured')
        else:
            trig_cfg[k] = combined_cfg[k]

    return trig_cfg, dumper_cfg


##############################################################################
###################### internal functions ####################################
##############################################################################

def getLabelingBuilderAlg(cfgFlags):
    label_tools = []
    for ptype in ['BHadrons','CHadrons','Taus']:
        tool = CompFactory.CopyFlavorLabelTruthParticles(f'{ptype}Builder')
        tool.ParticleType = f'{ptype}Final'
        tool.OutputName = f'TruthLabel{ptype}Final'
        tool.OutputLevel = cfgFlags.Exec.OutputLevel
        label_tools.append(tool)
    labelCollectionBuilder = CompFactory.JetAlgorithm("LabelBuilderAlg")
    labelCollectionBuilder.Tools = label_tools
    labelCollectionBuilder.OutputLevel = cfgFlags.Exec.OutputLevel
    return labelCollectionBuilder


def triggerJetGetterCfg(cfgFlags, chain, temp_jets, data=False, temp_btag='',
                        additional_chains=[]):
    ca = ComponentAccumulator()

    # This is (was?) needed for TDT
    ca.merge(MetaDataSvcCfg(cfgFlags))
    tdtca = TrigDecisionToolCfg(cfgFlags)
    ca.merge(tdtca)

    # Local component to move trigger elements into collections so we
    # can access them inside offline code
    jetGetter = CompFactory.TriggerJetGetterAlg(
        f'TriggerJetGetter_{chain}')
    jetGetter.triggerDecisionTool = tdtca.getPrimary()
    jetGetter.bJetChain = chain
    jetGetter.additionalChains = additional_chains
    jetGetter.outputJets = temp_jets
    jetGetter.outputBTag = temp_btag
    if not data: 
        jetGetter.jetModifiers = [getJetDeltaRFlavorLabelTool()]
    jetGetter.TrigJetCollectionKey = "HLT_.*"
    ca.addEventAlgo(jetGetter)

    return ca


def offlineBTagMatcherCfg(
        flags,
        btag_key,
        jet_key,
        taggers_to_copy=['DL1r','DL1dv00'],
        truth_labels=[
            'HadronConeExclTruthLabelID',
            'HadronConeExclExtendedTruthLabenlID',
        ],
        source_btag_key='BTagging_AntiKt4EMPFlow',
        source_jet_key='AntiKt4EMPFlowJets'):

    ca = ComponentAccumulator()
        # match to offline jets, pull out some info
    matcher = CompFactory.TriggerBTagMatcherAlg('matcher')
    matcher.offlineBtagKey = source_btag_key
    matcher.triggerBtagKey = btag_key
    matcher.floatsToCopy = {
        f'{tagger}_p{flav}':f'OfflineMatched{tagger}_p{flav}' for tagger in taggers_to_copy for flav in 'bcu'
    }
    matcher.offlineJetKey = source_jet_key
    matcher.triggerJetKey = jet_key
    truth_labels = [
        'HadronConeExclTruthLabelID',
        'HadronConeExclExtendedTruthLabelID',
    ]
    matcher.jetIntsToCopy = {
        x:f'OfflineMatched{x}' for x in truth_labels
    }
    ca.addEventAlgo(matcher)
    return ca


def jetMatcherCfg(
        flags,
        target_jets,
        source_jets,
        fast_dips=[],
        float_map={},
        iparticles=[],
        match_suffix='MatchedJet'
    ):
    ca = ComponentAccumulator()
    fd_vars = {f'{t}_p{x}':f'{t}_p{x}' for x in 'cub' for t in fast_dips}
    iparticlesToCopy={x:x for x in iparticles}
    matcher = CompFactory.JetMatcherAlg(
        name=f'Matcher_{target_jets}_from_{source_jets}',
        targetJet=target_jets,
        sourceJets=[source_jets],
        floatsToCopy=(fd_vars | float_map),
        iparticlesToCopy=iparticlesToCopy,
        dR=f'deltaRTo{match_suffix}',
        dPt=f'deltaPtTo{match_suffix}',
    )
    ca.addEventAlgo(matcher)
    return ca


##############################################################################
###################### top level functions ###################################
##############################################################################

def pflowDumper(cfgFlags, chain, additional_chains=[], data=False, mc21=False, offline_jets=True, do_track_matching=True, emtopo_jets=True,mc24=False):

    # this thing only exists in full athena, thus the import here
    from BTagging.BTagTrackAugmenterAlgConfig import BTagTrackAugmenterAlgCfg

    ca = ComponentAccumulator()

    # build labeling collections
    if not data:
        labelAlg = getLabelingBuilderAlg(cfgFlags)
        ca.addEventAlgo(labelAlg)

    # get jets
    temp_btag, temp_jets = 'tempBtag', 'tempJets'
    ca.merge(triggerJetGetterCfg(
        cfgFlags,
        chain=chain,
        additional_chains=additional_chains,
        temp_jets=temp_jets,
        temp_btag=temp_btag,
    ))

    # match to offline jets, pull out some info
    if offline_jets:
        ca.merge(offlineBTagMatcherCfg(
            cfgFlags,
            btag_key=temp_btag,
            jet_key=temp_jets))
    # match to associated pflow jets
    fastTaggers=['fastDIPS20211215','dips20211116']
    if not mc21:
        fastTaggers += ['GN120230331']
    if mc24:
        fastTaggers += ['tlaGN220240122']
    ca.merge(jetMatcherCfg(
        cfgFlags, temp_jets,
        source_jets='HLT_AntiKt4EMPFlowJets_subresjesgscIS_ftf',
        fast_dips=fastTaggers,
        iparticles=[], # 'TracksForMinimalJetTag' to grab TLA tracks (needs recent sample)
        match_suffix='PFlowJet',
    ))
    # match to associated EMTopo jets
    if emtopo_jets:
        ca.merge(jetMatcherCfg(
            cfgFlags, temp_jets,
            source_jets='HLT_AntiKt4EMTopoJets_subjesIS_fastftag',
            fast_dips=['fastDips'],
            iparticles=[],
            match_suffix='EMTopoJet',
        ))

    # associate fullscan tracks to the jets
    if do_track_matching:
        fs_tracks = 'HLT_IDTrack_FS_FTF'
        fs_vertices = 'HLT_IDVertex_FS'
        ca.merge(BTagTrackAugmenterAlgCfg(
                cfgFlags,
                TrackCollection=fs_tracks,
                PrimaryVertexCollectionName=fs_vertices))
        ca.addEventAlgo(CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
            name='_'.join(['PoorMansIpAugmenter',fs_tracks, fs_vertices]),
            trackContainer=fs_tracks,
            primaryVertexContainer=fs_vertices))
        ca.merge(JetParticleAssociationAlgCfg(
            cfgFlags,
            JetCollection=temp_jets,
            InputParticleCollection=fs_tracks,
            OutputParticleDecoration='FsTracks'))


    return ca


def emtopoDumper(cfgFlags, chain, data=False, additional_chains=[], mc21=False, offline_jets=True, do_track_matching=True,emtopo_jets=True,mc24=False):
    if not emtopo_jets:
        raise("ERROR: Trying to dump emtopo jets with emtopo_jets=False !")

    ca = ComponentAccumulator()


    # build labeling collections
    if not data:
        labelAlg = getLabelingBuilderAlg(cfgFlags)
        ca.addEventAlgo(labelAlg)

    # get jets
    temp_jets = 'tempEmtopoJets'
    ca.merge(triggerJetGetterCfg(
        cfgFlags,
        chain=chain,
        data=data,
        additional_chains=additional_chains,
        temp_jets=temp_jets
    ))

    # associate fullscan tracks to the jets
    if do_track_matching:
        tracks = 'HLT_IDTrack_JetSuper_FTF'
        vertices = 'HLT_IDVertex_JetSuper'  if not mc21 else ''
        for tr in [tracks]:
            ca.addEventAlgo(
                CompFactory.FlavorTagDiscriminants.PoorMansIpAugmenterAlg(
                    name='_'.join(['PoorMansIpAugmenter', tr,vertices]),
                    trackContainer=tr,
                    primaryVertexContainer=vertices,
                ))

    # get the matched pileup jet PVZ
    if not data and not mc21 and offline_jets:
        ca.addEventAlgo(
            CompFactory.TruthJetPrimaryVertexDecoratorAlg(
                name="TruthJetPrimaryVertexDecorator",
                jetPvDecorator="AntiKt4TruthJets.PVz",
            )
        )
        ca.addEventAlgo(
            CompFactory.JetMatcherAlg(
                name=f'Matcher{temp_jets}PVz',
                targetJet=temp_jets,
                sourceJets=['InTimeAntiKt4TruthJets', 'AntiKt4TruthJets'],
                floatsToCopy={'PVz':'TruthJetPVz'},
                dR='deltaRToTruthJet',
                dPt='deltaPtToTruthJet',
                ptPriorityWithDeltaR=0.3,
                sourceMinimumPt=10000.,
            )
        )


    return ca
