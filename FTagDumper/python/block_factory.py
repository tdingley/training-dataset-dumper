from FTagDumper import blocks


class BlockFactory:
    block_map = {name: getattr(blocks, name) for name in blocks.__all__}

    def __init__(self, config, cfgFlags):
        self.dumper_config = config.get("dumper", config)
        self.blocks = config.pop("ca_blocks", [])
        self.cfgFlags = cfgFlags

    def create_block(self, name, dumper_config, block_kwargs):
        """Create a block from its name and configuration."""
        block_class = self.block_map.get(name)
        if not block_class:
            raise ValueError(f"Unknown block in config: {name}")
        try:
            block = block_class(dumper_config=dumper_config, cfgFlags=self.cfgFlags, **block_kwargs)
        except TypeError as e:
            orig = f"{block_class.__name__}.{e}"
            raise TypeError(
                f"Error creating block '{name}' with arguments {block_kwargs}.\n\t{orig}"
            ) from e
        return block

    def __iter__(self):
        """Loop over a list of block configs and yeild the the created blocks."""
        for block_kwargs in self.blocks:
            block_name = block_kwargs.pop("block")
            yield self.create_block(block_name, self.dumper_config, block_kwargs)
