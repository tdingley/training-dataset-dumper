#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ToolHandle.h"
#include "InDetTrackSystematicsTools/InDetTrackSmearingTool.h"
#include "InDetTrackSystematicsTools/InDetTrackTruthFilterTool.h"
#include "InDetTrackSystematicsTools/InDetTrackBiasingTool.h"
#include "InDetTrackSystematicsTools/InDetTrackSystematics.h"
#include "InDetTrackSystematicsTools/JetTrackFilterTool.h"

#include "xAODCore/ShallowCopy.h"
#include "xAODCore/ShallowAuxInfo.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticleAuxContainer.h"

/*
   @class TrackSystematicsAlg
   Algorithms that reads track container from StoreGate,
   create a copy of the tracks, apply tracking 
   variations and saves back the altered tracks.
   Original author:
   @author Remi Zaidan (remi.zaidan@cern.ch)
   @author Felix Clark (michael.ryan.clark@cern.ch)
   Rel22 author:
   @author Bingxuan Liu (bingxuan.liu@cern.ch)
*/

class TrackSystematicsAlg :  public AthAlgorithm { 
public:
  
  /** Constructors and destructors */
  TrackSystematicsAlg(const std::string& name, ISvcLocator *pSvcLocator);
  virtual ~TrackSystematicsAlg();
  
  /** Main routines specific to an ATHENA algorithm */
  virtual StatusCode initialize();
  virtual StatusCode execute();
  virtual StatusCode finalize();
  
private:
  
  std::string m_track_collection;
  std::string m_output_track_collection;

  std::string m_jet_collection;
  bool m_doJets = false; // set to false by deault

  ToolHandle< InDet::InDetTrackSmearingTool > m_track_smearing_tool; 
  ToolHandle< InDet::InDetTrackBiasingTool > m_track_biasing_tool {this, "track_biasing_tool", "", "track biasing tool"};
  ToolHandle< InDet::InDetTrackTruthFilterTool > m_track_truth_filter_tool;
  ToolHandle< InDet::JetTrackFilterTool > m_jet_track_filter_tool;

  std::vector< std::string > m_syst_names;

}; // class TrackSystematicsAlg


