#ifndef HIT_WRITER_HH
#define HIT_WRITER_HH

#include "AthContainers/AuxElement.h"
#include "HitWriterConfig.hh"
// Eigen needed for Vector3D
#include "GeoPrimitives/GeoPrimitives.h"

// Standard Library things
#include <string>
#include <vector>
#include <memory>


namespace H5 {
  class Group;
}

namespace xAOD {
  class Jet_v1;
  typedef Jet_v1 Jet;
  class TrackMeasurementValidation_v1;
  typedef TrackMeasurementValidation_v1 TrackMeasurementValidation;
  class Vertex_v1;
  typedef Vertex_v1 Vertex;
  class EventInfo_v1;
  typedef EventInfo_v1 EventInfo;
}

class HitWriterConfig;
class HitOutputWriter;
class HitConsumers;

class HitWriter
{
public:
  using TMVV = std::vector<const xAOD::TrackMeasurementValidation*>;
  HitWriter(
    H5::Group& output_file,
    const HitWriterConfig&);
  ~HitWriter();
  HitWriter(HitWriter&) = delete;
  HitWriter operator=(HitWriter&) = delete;
  HitWriter(HitWriter&&);
  // writer for cases where a primary vertex exists
  void write(
    const std::vector<const xAOD::TrackMeasurementValidation*>& hits,
    const xAOD::Jet& jet,
    const xAOD::Vertex& vx);
  // writer using only the beamspot
  void write(
    const std::vector<const xAOD::TrackMeasurementValidation*>& hits,
    const xAOD::Jet& jet,
    const xAOD::EventInfo& beamspot);
  // dummy writer
  void write_dummy();

private:
  void write_generic(
    const std::vector<const xAOD::TrackMeasurementValidation*>& hits,
    const xAOD::Jet& jet,
    const Amg::Vector3D& origin);
  TMVV sortHitsByDR(
    const TMVV& hits,
    const xAOD::Jet& jet,
    const xAOD::Vertex& vx);
  TMVV sortHitsByDPhi(
    const TMVV& hits,
    const xAOD::Jet& jet,
    const Amg::Vector3D& beamspot);
  template <typename T>
  using Acc = SG::AuxElement::ConstAccessor<T>;
  void add_hit_variables(HitConsumers&, const HitWriterConfig&);
  std::unique_ptr<HitOutputWriter> m_hdf5_hit_writer;
  float m_dist_to_jet;
  bool m_save_endcap_hits;
  bool m_save_only_clean_hits;
  HitWriterConfig::Save m_save_hits;
  Acc<int> m_HitContained;
  Acc<int> m_bec;
};

#endif
