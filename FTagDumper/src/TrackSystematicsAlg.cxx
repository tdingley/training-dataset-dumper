/* Migrated from https://gitlab.cern.ch/atlas-ftag-calibration/BTagTrackSystematicsAlgs/-/blob/main/src/TrackSystematicsAlg.cxx by bingxuan.liu@cern.ch. 
Emily Charlotte Graham migrated it from SVN to git. Original author is???? 
Only the smearing tool is included at the moment, updated to rel22.
Please note that the bias tool is currently disabled due to missing Run3 maps and bug in R22 z0 map.
*/

#include "TrackSystematicsAlg.h"

TrackSystematicsAlg::TrackSystematicsAlg(const std::string& name, ISvcLocator *pSvcLocator): 
  AthAlgorithm(name, pSvcLocator), 
  m_track_collection("InDetTrackParticles"), 
  m_output_track_collection("InDetTrackParticlesModified"), 
  m_track_smearing_tool("InDet::InDetTrackSmearingTool"),
  m_track_biasing_tool(),
  m_track_truth_filter_tool("InDet::InDetTrackTruthFilterTool"),
  m_jet_track_filter_tool("InDet::JetTrackFilterTool")
{
  declareProperty( "track_collection", m_track_collection);
  declareProperty( "jet_collection", m_jet_collection);
  declareProperty( "output_track_collection", m_output_track_collection);
  declareProperty( "systematic_variations", m_syst_names);
  declareProperty( "track_smearing_tool", m_track_smearing_tool);
  declareProperty( "track_truth_filter_tool", m_track_truth_filter_tool);
  declareProperty( "jet_track_filter_tool", m_jet_track_filter_tool);
  declareProperty( "track_biasing_tool", m_track_biasing_tool);
}

TrackSystematicsAlg::~TrackSystematicsAlg() = default;

StatusCode TrackSystematicsAlg::initialize() {

  ATH_CHECK( m_track_smearing_tool.retrieve() );
  if (!m_track_biasing_tool.empty()) {
    ATH_CHECK( m_track_biasing_tool.retrieve() );
  }
  ATH_CHECK( m_track_truth_filter_tool.retrieve() );
  ATH_CHECK( m_jet_track_filter_tool.retrieve() );

  CP::SystematicSet active_systs;

  for (const auto& name : m_syst_names) {
    
    ATH_MSG_INFO("Adding systematic variation "<< name);
    active_systs.insert(CP::SystematicVariation(name));
    
    // Check whether TIDE systematic is included. In this case, set m_doJets to true.
    if (name.find("TIDE") != std::string::npos) m_doJets = true;
  }

  auto code_smear = m_track_smearing_tool->applySystematicVariation(active_systs);
  if (code_smear != StatusCode::SUCCESS) {
    ATH_MSG_ERROR("Failed to apply track variation to " << m_track_smearing_tool);
    return StatusCode::FAILURE;
  }
  
  if (!m_track_biasing_tool.empty()) {
    auto code_bias = m_track_biasing_tool->applySystematicVariation(active_systs);
    if (code_bias != StatusCode::SUCCESS) {
        ATH_MSG_ERROR("Failed to apply track variation to " << m_track_biasing_tool);
        return StatusCode::FAILURE;
    }
  }

  auto code_filter = m_track_truth_filter_tool->applySystematicVariation(active_systs);
  if (code_filter != StatusCode::SUCCESS) {
    ATH_MSG_ERROR( "Failed to apply systematic variation to " << m_track_truth_filter_tool );
    return StatusCode::FAILURE;
  }
 
  auto code_jet_filter = m_jet_track_filter_tool->applySystematicVariation(active_systs);
  if (code_jet_filter != StatusCode::SUCCESS) {
    ATH_MSG_ERROR( "Failed to apply systematic variation to " << m_jet_track_filter_tool );
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}

StatusCode TrackSystematicsAlg::execute() {

  const xAOD::TrackParticleContainer *tracks = nullptr;
  ATH_CHECK( evtStore()->retrieve( tracks, m_track_collection) );

  const xAOD::JetContainer* jets = nullptr;
  if (m_doJets)  ATH_CHECK( evtStore()->retrieve( jets, m_jet_collection));

  auto new_tracks = std::make_unique<xAOD::TrackParticleContainer>();
  auto new_tracks_aux = std::make_unique<xAOD::AuxContainerBase>();
  new_tracks->setStore(new_tracks_aux.get());

  using Link_t = ElementLink< xAOD::TrackParticleContainer >;
  using Deco_t = SG::AuxElement::Decorator< Link_t >;
  static Deco_t setOriginLink("originalTrackLink");
  static Deco_t setModifiedLink("correctedTrackLink");

  for ( const xAOD::TrackParticle* track : *tracks ) {
   
    // Apply the efficiency/fake ratesystematics. TIDE systematics are only applied to high pT jets.

    if( !m_track_truth_filter_tool->accept(track) ) continue;
    if( m_doJets && !m_jet_track_filter_tool->accept(track, jets) ) continue;

    auto* new_track = new_tracks->emplace_back(new xAOD::TrackParticle());
    *new_track = *track; // copy over the information

    // apply smearing tool to the track
    if (m_track_smearing_tool->applyCorrection(*new_track) == CP::CorrectionCode::Error) {
      ATH_MSG_ERROR( "Could not apply InDetTrackSmearingTool." );
      return StatusCode::FAILURE;
    }

    if (!m_track_biasing_tool.empty()) {
        // apply biasing tool to the track
        if (m_track_biasing_tool->applyCorrection(*new_track) == CP::CorrectionCode::Error) {
        ATH_MSG_ERROR( "Could not apply InDetTrackBiasingTool." );
        return StatusCode::FAILURE;
        }
    }

    Link_t origin_link(*tracks, track->index());
    setOriginLink(*new_track) = origin_link;
      
    Link_t modified_link(*new_tracks, new_track->index());
    setModifiedLink(*track) = modified_link;

  }

  ATH_MSG_DEBUG( tracks->size() << " / " << new_tracks->size() << " \toriginal / modified tracks" );
  ATH_CHECK( evtStore()->record(std::move(new_tracks), m_output_track_collection) );
  ATH_CHECK( evtStore()->record(std::move(new_tracks_aux), m_output_track_collection+"Aux.") ); 

  return StatusCode::SUCCESS;
}

StatusCode TrackSystematicsAlg::finalize(){
  ATH_CHECK(m_track_smearing_tool->release());
  if (!m_track_biasing_tool.empty()) {
    ATH_CHECK(m_track_biasing_tool->release());
  }
  ATH_CHECK(m_track_truth_filter_tool->release());
  ATH_CHECK(m_jet_track_filter_tool->release());
  return StatusCode::SUCCESS;
}

